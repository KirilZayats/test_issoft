import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class OrdersItemsParser {
    public HashMap<String, Map<String, Integer>> loadAndParse(String fileName) {
        HashMap<String, Map<String, Integer>> parsedData = new HashMap<>();

        try (BufferedReader src = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = src.readLine()) != null) {
                Map<String, Integer> item = new HashMap<>();
                String[] idData = line.split(",");
                try {
                    item.put(idData[1], Integer.valueOf(idData[2]));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
                parsedData.put(idData[0], item);
            }

        } catch (IOException e) {
            System.out.println("Error with file work '" + fileName + "'.");
        }
        return  parsedData;
    }
}
