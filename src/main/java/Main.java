import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        OrderDataParser orderDataParser = new OrderDataParser();
        HashMap<String, LocalDateTime> orders = orderDataParser.loadAndParse("orders.csv");
        OrdersItemsParser ordersItemsParser = new OrdersItemsParser();
        HashMap<String, Map<String, Integer>> ordersItems = ordersItemsParser.loadAndParse("order_items.csv");
        HashMap<String, Map<String, Integer>> products = ordersItemsParser.loadAndParse("products.csv");
        for (int j = 1; j < 29; j++) {
            Set<String> ordersId = new HashSet<>();
            for (Map.Entry<String, LocalDateTime> order : orders.entrySet()) {
                if (order.getValue().getDayOfMonth() == j) ordersId.add(order.getKey());
            }
            HashMap<String, Integer> productTable = new HashMap<>();
            for (Map.Entry<String, Map<String, Integer>> orderItem : ordersItems.entrySet()) {
                if (ordersId.contains(orderItem.getKey())) {
                    if (!productTable.containsKey(orderItem.getValue().keySet().toArray()[0])) {
                        productTable.put((String) orderItem.getValue().keySet().toArray()[0],
                                (Integer) orderItem.getValue().values().toArray()[0]);
                    } else {
                        productTable.put((String) orderItem.getValue().keySet().toArray()[0],
                                productTable.get((String) orderItem.getValue().keySet().toArray()[0]) + (Integer) orderItem.getValue().values().toArray()[0]);
                    }
                }
            }
            int profit = 0;
            String winner = "";
            for (Map.Entry<String, Integer> product : productTable.entrySet()) {
                int timely = product.getValue() * (Integer) products.get(product.getKey()).values().toArray()[0];
                if (timely >= profit) {
                    profit = timely;
                    winner = (String) products.get(product.getKey()).keySet().toArray()[0];
                }
            }
            System.out.println(j + "-01-2021 " + winner + " " + profit + "(" + ordersId.size() + ")");
        }

    }
}
