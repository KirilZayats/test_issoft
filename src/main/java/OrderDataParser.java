import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.HashMap;

public class OrderDataParser {

    public HashMap<String, LocalDateTime> loadAndParse(String fileName) {
        HashMap<String, LocalDateTime> parsedData = new HashMap<>();
        try (BufferedReader src = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = src.readLine()) != null) {
                String[] idData = line.split(",");
                try {
                    parsedData.put(idData[0], LocalDateTime.parse(idData[1]));
                } catch (DateTimeParseException e) {
                    e.printStackTrace();
                }
            }

        } catch (IOException e) {
            System.out.println("Error with file work '" + fileName + "'.");
        }
        return parsedData;
    }
}
